﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Client.ViewModels
{
    class VehicleTopUpViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("sum")]
        public decimal Sum { get; set; }
    }
}
