﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Client.ViewModels
{
    public class VehicleViewModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("vehicleType")]
        public VehicleType VehicleType { get; set; }

        [JsonProperty("balance")]
        public decimal Balance { get; set; }

        public override string ToString()
        {
            return string.Format("[{0}] | {1} | Balance = {2}", Id.ToString(), VehicleType.ToString(), Balance.ToString());
        }
    }
}
