﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CoolParking.Client.ViewModels
{
    class TransactionsViewModel
    {
        [JsonProperty("sum")]
        public decimal Sum { get; set; }
        [JsonProperty("vehicleId")]
        public string VehicleId{ get; set; }
        [JsonProperty("transactionDate")]
        public DateTime TransactionDate { get; set; }

        public override string ToString()
        {
            return string.Format($"{TransactionDate.ToString("MM:dd:yyyy hh:mm:ss tt")}: " +
                $"{Sum} money withdrawn from vehicle with Id={VehicleId}");
        }
    }
}
