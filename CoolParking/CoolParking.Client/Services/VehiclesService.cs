﻿using CoolParking.BL.Models;
using CoolParking.Client.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;

namespace CoolParking.Client.Services
{
    class VehiclesService
    {
        private string _baseUrl = "";
        private HttpClient _client;

        public VehiclesService(string baseUrl, HttpClient client)
        {
            _baseUrl = baseUrl;
            _client = client;
        }

        public async Task<IEnumerable<VehicleViewModel>> GetVehicles()
        {
            var response = await _client.GetAsync($"{_baseUrl}/api/vehicles");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<IEnumerable<VehicleViewModel>>();
        }

        public async Task<VehicleViewModel> GetVehicle(string id)
        {
            var response = await _client.GetAsync($"{_baseUrl}/api/vehicles/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<VehicleViewModel>();
        }

        public async Task<VehicleViewModel> PostVehicle(VehicleViewModel vehicle)
        {
            var response = await _client.PostAsJsonAsync($"{_baseUrl}/api/vehicles", vehicle);
            if(response.StatusCode != System.Net.HttpStatusCode.Created)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<VehicleViewModel>();
        }

        public async Task DeleteVehicle(string id)
        {
            var response = await _client.DeleteAsync($"{_baseUrl}/api/vehicles/{id}");
            if (response.StatusCode != System.Net.HttpStatusCode.NoContent)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
        }
    }
}
