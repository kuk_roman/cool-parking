﻿using CoolParking.Client.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client.Services
{
    class TransactionsService
    {
        private string _baseUrl = "";
        private HttpClient _client;

        public TransactionsService(string baseUrl, HttpClient client)
        {
            _baseUrl = baseUrl;
            _client = client;
        }

        public async Task<IEnumerable<TransactionsViewModel>> GetLastParkingTransactions()
        {
            var response = await _client.GetAsync($"{_baseUrl}/api/transactions/last");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<IEnumerable<TransactionsViewModel>>();
        }

        public async Task<string> GetAllTransactions()
        {
            var response = await _client.GetAsync($"{_baseUrl}/api/transactions/all");
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<string>();
        }

        public async Task<VehicleViewModel> TopUpVehicle(VehicleTopUpViewModel vehicleTopUp)
        {
            var response = await _client.PutAsJsonAsync($"{_baseUrl}/api/topUpVehicle", vehicleTopUp);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"{response.StatusCode} : {await response.Content.ReadAsStringAsync()}");
            }
            return await response.Content.ReadAsAsync<VehicleViewModel>();
        }
    }
}
