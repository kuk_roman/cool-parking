﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.Client.Services
{
    class ParkingService
    {

        private string _baseUrl = "";
        private HttpClient _client;

        public ParkingService(string baseUrl, HttpClient client)
        {
            _baseUrl = baseUrl;
            _client = client;
        }

        public async Task<decimal> GetBalance()
        {
            var balance = await _client.GetStringAsync($"{_baseUrl}/api/parking/balance");
            return JsonConvert.DeserializeObject<decimal>(balance);
        }

        public async Task<int> GetCapacity()
        {
            var capacity = await _client.GetStringAsync($"{_baseUrl}/api/parking/capacity");
            return JsonConvert.DeserializeObject<int>(capacity);
        }

        public async Task<int> GetFreePlaces()
        {
            var freePlaces = await _client.GetStringAsync($"{_baseUrl}/api/parking/freePlaces");
            return JsonConvert.DeserializeObject<int>(freePlaces);
        }

    }
}
