﻿using CoolParking.Client.Services;
using CoolParking.Client.ViewModels;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoolParking.Client
{
    class Program
    {
        private static readonly string _baseUrl = "http://localhost:51188";
        static async Task Main(string[] args)
        {
            var httpClient = new HttpClient();
            var parkingClient = new ParkingService(_baseUrl, httpClient);
            var vehicleClient = new VehiclesService(_baseUrl, httpClient);
            var transactionsClient = new TransactionsService(_baseUrl, httpClient);
        }
           
    }
}
