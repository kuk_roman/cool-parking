﻿using CoolParking.BL.Models;
using System.ComponentModel.DataAnnotations;

namespace CoolParking.WebAPI.ViewModels
{
    public class VehicleViewModel
    {
        [Required]
        public string Id { get; set; }
        [Required]
        public VehicleType VehicleType { get; set; }
        [Required]
        public decimal Balance { get; set; }

    }

}
