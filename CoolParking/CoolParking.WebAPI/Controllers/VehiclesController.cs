﻿using System;
using System.Collections.Generic;
using System.Linq;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.WebAPI.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly IParkingService parkingService;

        public VehiclesController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Vehicle>> GetVehicles()
        {
            return Ok(parkingService.GetVehicles());
        }

        [HttpGet("{id}")]
        public ActionResult<Vehicle> GetVehicle(string id)
        {
            try
            {
                Vehicle.IdValidation(id);
                var vehicle = parkingService.GetVehicles().Where(v => v.Id == id).FirstOrDefault();
                return Ok(vehicle);
            }
            catch (ArgumentOutOfRangeException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (NullReferenceException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpPost]
        public ActionResult<Vehicle> PostVehicle(VehicleViewModel vehicleViewModel)
        {
            try
            {
                parkingService.AddVehicle(new Vehicle(vehicleViewModel.Id, vehicleViewModel.VehicleType, vehicleViewModel.Balance));
                return Created($"api/vehicles/{vehicleViewModel.Id}", vehicleViewModel);
            }
            catch (NotSupportedException)
            {
                return BadRequest("Invalid request body");
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteVehicle(string id)
        {
            try
            {
                Vehicle.IdValidation(id);
                parkingService.RemoveVehicle(id);
                return NoContent();
            }
            catch (ArgumentOutOfRangeException e)
            {
                return BadRequest(e.Message);
            }
            catch (ArgumentException e)
            {
                return NotFound(e.Message);
            }
            catch (NullReferenceException e)
            {
                return NotFound(e.Message);
            }
        }

    }

}
