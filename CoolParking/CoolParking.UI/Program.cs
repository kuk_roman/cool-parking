﻿using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System;
using System.Timers;

namespace CoolParking.UI
{
    class Program
    {
        static void Main(string[] args)
        {

            var parking = new ParkingUI();

            while (true)
            {
                Console.WriteLine("Select a command:");
                Console.WriteLine("1 - Print parking balance");
                Console.WriteLine("2 - Print current income period");
                Console.WriteLine("3 - Print free parking places");
                Console.WriteLine("4 - Print current transactions period");
                Console.WriteLine("5 - Print transaction history");
                Console.WriteLine("6 - Print vehicles list");
                Console.WriteLine("7 - Add a vehicle");
                Console.WriteLine("8 - Remove a vehicle");
                Console.WriteLine("9 - Top up a vehicle balance");
                Console.WriteLine("0 - Exit");
                Console.WriteLine();

                var commandString = Console.ReadLine();

                Console.WriteLine();

                try
                {
                    int command = int.Parse(commandString);

                    switch (command)
                    {
                        case 0:
                            return;
                        case 1:
                            parking.PrintParkingBalance();
                            break;
                        case 2:
                            parking.PrintCurrentPeriodIncome();
                            break;
                        case 3:
                            parking.PrintParkingFreePlaces();
                            break;
                        case 4:
                            parking.PrintCurrentPeriodTransactions();
                            break;
                        case 5:
                            parking.PrintTransactionHistory();
                            break;
                        case 6:
                            parking.PrintVehiclesList();
                            break;
                        case 7:
                            parking.AddVehicle();
                            break;
                        case 8:
                            parking.RemoveVehicle();
                            break;
                        case 9:
                            parking.TopUpVehicleBalance();
                            break;
                        default:
                            throw new FormatException();
                    }

                }
                catch (FormatException)
                {
                    Console.WriteLine($"Incorrect command");
                }
                Console.WriteLine();
            }
        }

    }
}
