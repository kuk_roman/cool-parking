﻿using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace CoolParking.UI
{
    class ParkingUI
    {
        private readonly string logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";

        private readonly ParkingService parkingService;
        private readonly TimerService withdrawTimer;
        private readonly TimerService logTimer;
        private readonly LogService logService;

        public ParkingUI()
        {
            File.WriteAllText(logFilePath, string.Empty);
            withdrawTimer = new TimerService(TimeSpan.FromSeconds(Settings.WithdrawTime).TotalMilliseconds);
            logTimer = new TimerService(TimeSpan.FromSeconds(Settings.LogTime).TotalMilliseconds);
            logService = new LogService(logFilePath);
            parkingService = new ParkingService(withdrawTimer, logTimer, logService);
            withdrawTimer.Start();
            logTimer.Start();
        }

        public void PrintParkingBalance()
        {
            Console.WriteLine("Current parking balance = {0}", parkingService.GetBalance().ToString());
        }

        public void PrintCurrentPeriodIncome()
        {
            decimal income = 0.0M;
            foreach (var transaction in parkingService.GetLastParkingTransactions())
            {
                income += transaction.Sum;
            }

            Console.WriteLine("Current period income = {0}", income);
        }

        public void PrintParkingFreePlaces()
        {
            Console.WriteLine("{0} out of {1} places are free.", parkingService.GetFreePlaces(), parkingService.GetCapacity());
        }

        public void PrintCurrentPeriodTransactions()
        {
            if (parkingService.GetLastParkingTransactions().Length == 0)
            {
                Console.WriteLine("There is no transaction in current period!");
                return;
            }
            Console.WriteLine("Current period transactions:");
            foreach (var transaction in parkingService.GetLastParkingTransactions())
            {
                Console.WriteLine(transaction.ToString());
            }
        }

        public void PrintTransactionHistory()
        {
            if(parkingService.ReadFromLog().Length == 0)
            {
                Console.WriteLine("There is no transaction in history!");
                return;
            }
            Console.WriteLine("Transaction history:");
            Console.WriteLine(parkingService.ReadFromLog());
        }

        public void PrintVehiclesList()
        {
            if (parkingService.GetVehicles().Count == 0)
            {
                Console.WriteLine("There is no vehicle on parking!");
                return;
            }
            Console.WriteLine("Vehicles on parking:");
            foreach (var vehicle in parkingService.GetVehicles())
            {
                Console.WriteLine(vehicle.ToString());
            }
        }

        public void AddVehicle()
        {
            Console.WriteLine("Select a vehicle type:");

            Console.WriteLine("\t1 - {0}", VehicleType.PassengerCar.ToString());
            Console.WriteLine("\t2 - {0}", VehicleType.Truck.ToString());
            Console.WriteLine("\t3 - {0}", VehicleType.Bus.ToString());
            Console.WriteLine("\t4 - {0}", VehicleType.Motorcycle.ToString());

            var vehicleTypeString = Console.ReadLine();
            Console.WriteLine("Enter an initial vehicle balance:");
            var balanceString = Console.ReadLine();
            try
            {
                int vehicleType = int.Parse(vehicleTypeString) - 1;
                decimal balance = decimal.Parse(balanceString);

                parkingService.AddVehicle(new Vehicle((VehicleType)vehicleType, balance));
            }
            catch (FormatException e)
            {
                Console.WriteLine($"Unable to parse '{e.Data}'");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public void RemoveVehicle()
        {
            if (parkingService.GetVehicles().Count == 0)
            {
                Console.WriteLine("There is no vehicle on parking!");
                return;
            }

            Console.WriteLine("Enter id of the vehicle you want to remove:");
            var id = Console.ReadLine();
            try
            {
                parkingService.RemoveVehicle(id);
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e.Message);
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void TopUpVehicleBalance()
        {
            if(parkingService.GetVehicles().Count == 0)
            {
                Console.WriteLine("There is no vehicle on parking!");
                return;
            }

            Console.WriteLine("Enter id of the vehicle you want to top up:");
            var id = Console.ReadLine();
            Console.WriteLine("Enter sum to top up:");
            var sumString = Console.ReadLine();
            try
            {
                decimal sum = decimal.Parse(sumString);
                parkingService.TopUpVehicle(id, sum);
            }
            catch (FormatException e)
            {
                Console.WriteLine($"Unable to parse '{e.Data}'");
            }
            catch (ArgumentException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
