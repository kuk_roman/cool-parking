﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using System;
using System.Collections.Generic;

namespace CoolParking.BL.Models 
{
    class Parking: IDisposable
    {
        private static readonly Lazy<Parking> parking = new Lazy<Parking>(() => new Parking());
        public static Parking Instance { get { return parking.Value; } }

        public List<Vehicle> Vehicles { get; internal set; }
        public decimal Balance { get; internal set; }
        public int Capacity { get; }
        public Dictionary<VehicleType, decimal> Tariffs { get; private set; }
        public decimal FeeCoeficient { get; }
        public List<TransactionInfo> Transactions { get; set; }

        private Parking()
        {
            Vehicles = new List<Vehicle>();
            Balance = Settings.Balance;
            Capacity = Settings.Capacity;
            Tariffs = Settings.Tariffs;
            FeeCoeficient = Settings.FeeCoeficient;
            Transactions = new List<TransactionInfo>();
        }

        public void Dispose()
        {
            Vehicles.Clear();
            Balance = 0;
        }

    }
}
