﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models 
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; internal set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            IdValidation(id);
            if (!Enum.IsDefined(typeof(VehicleType), vehicleType))
            {
                throw new ArgumentException("Invalid vehicle type!");
            }
            if (balance < 0)
            {
                throw new ArgumentException("Vehicle balance must be positive!");
            }
            else
            {
                this.Id = id;
                this.VehicleType = vehicleType;
                this.Balance = balance;
            }
        }

        public Vehicle(VehicleType vehicleType, decimal balance) :
            this(GenerateRandomRegistrationPlateNumber(), vehicleType, balance) {}

        public static string GenerateRandomRegistrationPlateNumber()
        {
            StringBuilder result = new StringBuilder();
            result.Append(GenerateRandom.String(2, false));
            result.Append('-');
            result.Append(GenerateRandom.Number(Settings.MinId, Settings.MaxId));
            result.Append('-');
            result.Append(GenerateRandom.String(2, false));

            return result.ToString();
        }

        public static void IdValidation(string vehicleId)
        {
            if (!Regex.IsMatch(vehicleId, Settings.pattern))
            {
                throw new ArgumentOutOfRangeException("Incorrect vehicle Id format!");
            }
        }

        public override string ToString() => string.Format("[{0}] | {1} | Balance = {2}", Id.ToString(), VehicleType.ToString(), Balance.ToString());
    }

    class GenerateRandom
    {
        public static int Number(int minNumber, int maxNumber)
        {
            Random random = new Random();
            return random.Next(minNumber, maxNumber);
        }

        public static string String(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }
    }

}
