﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System.Collections.Generic;

namespace CoolParking.BL.Models 
{
    public static class Settings 
    {
        public static decimal Balance { get; } = 0;
        public static int Capacity { get; } = 10;
        public static int WithdrawTime { get; } = 5;
        public static int LogTime { get; } = 60;
        public static Dictionary<VehicleType, decimal> Tariffs { get; } = SetTarrifs();
        public static decimal FeeCoeficient { get; } = 2.5m;
        public static int MinId { get; } = 0001;
        public static int MaxId { get; } = 9999;

        public static readonly string pattern = @"\b[A-Z]{2}-\d{4}-[A-Z]{2}\b";

        private static Dictionary<VehicleType, decimal> SetTarrifs()
        {
            var tariffs = new Dictionary<VehicleType, decimal>(4);
            tariffs.Add(VehicleType.PassengerCar, 2);
            tariffs.Add(VehicleType.Truck, 5);
            tariffs.Add(VehicleType.Bus, 3.5m);
            tariffs.Add(VehicleType.Motorcycle, 1);

            return tariffs;
        }
    }
}
