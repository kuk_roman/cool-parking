﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.

using CoolParking.BL.Services;
using System;

namespace CoolParking.BL.Models 
{
    public class TransactionInfo
    {
        public string VehicleId { get; set; }
        public decimal Sum { get; set; }
        public DateTime TransactionDate { get; set; }
        private ParkingService ParkingService { get; }

        public TransactionInfo(string vehicleId, ParkingService parkingService)
        {
            VehicleId = vehicleId;
            TransactionDate = DateTime.Now;
            ParkingService = parkingService;
            Sum = CalculateSum();
        }

        public decimal CalculateSum()
        {
            var vehicle = ParkingService.FindVehicleById(VehicleId);
            if(vehicle != null)
            {
                decimal tariff = ParkingService.GetTariffById(vehicle.VehicleType);
                decimal fineCoef = ParkingService.GetFeeCoef();
                return vehicle.Balance >= tariff ? tariff : 
                    (vehicle.Balance >= 0 ? vehicle.Balance + (tariff - vehicle.Balance) * fineCoef : tariff * fineCoef);
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public override string ToString()
        {
            return string.Format($"{TransactionDate.ToString("MM/dd/yyyy hh:mm:ss tt")}: " +
                $"{Sum} money withdrawn from vehicle with Id='{VehicleId}'");
        }

    }
}
