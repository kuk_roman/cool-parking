﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService, IDisposable
    {
        public double Interval { get => SystemTimer.Interval; set => SystemTimer.Interval = value; }
        private Timer SystemTimer { get; }

        public event ElapsedEventHandler Elapsed
        {
            add { SystemTimer.Elapsed += value; }
            remove { SystemTimer.Elapsed -= value; }
        }

        public TimerService()
        {
            SystemTimer = new Timer();
        }

        public TimerService(double interval) : this()
        {
            Interval = interval;
        }

        public void Dispose()
        {
            SystemTimer.Dispose();
        }

        public void Start()
        {
            SystemTimer.Start();
        }

        public void Stop()
        {
            SystemTimer.Stop();
        }
    }
}

