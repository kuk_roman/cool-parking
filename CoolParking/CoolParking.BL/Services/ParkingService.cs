﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative Balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System.Collections.ObjectModel;
using System;
using System.Timers;
using System.Text;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking Parking { get; }
        public ITimerService WithdrawTimer { get; }
        public ITimerService LogTimer { get; }
        public ILogService LogService { get; }

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            WithdrawTimer = withdrawTimer;
            WithdrawTimer.Interval = TimeSpan.FromSeconds(Settings.WithdrawTime).TotalMilliseconds;
            WithdrawTimer.Elapsed += OnWithdraw;
            WithdrawTimer.Start();
            LogTimer = logTimer;
            LogTimer.Interval = TimeSpan.FromSeconds(Settings.LogTime).TotalMilliseconds;
            LogTimer.Elapsed += OnLog;
            LogTimer.Start();
            LogService = logService;
            Parking = Parking.Instance;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            var vehiclesWithThisID = Parking.Vehicles.FindAll(v => v.Id == vehicle.Id);
            if (GetFreePlaces() == 0)
            {
                throw new InvalidOperationException("There is no free places in parking!");
            }
            else if (vehiclesWithThisID.Count > 0)
            {
                throw new ArgumentException("Vehicle with such Id is already in parking!");
            } 
            else
            {
                Parking.Vehicles.Add(vehicle);
            }
        }

        public void Dispose()
        {
            WithdrawTimer.Dispose();
            LogTimer.Dispose();
            Parking.Dispose();
        }

        public decimal GetBalance()
        {
            return Parking.Balance;
        }

        public int GetCapacity()
        {
            return Parking.Capacity;
        }

        public int GetFreePlaces()
        {
            return Parking.Capacity - Parking.Vehicles.Count;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            if (GetVehicles().Count == 0)
                return new TransactionInfo[0];
            return Parking.Transactions.ToArray();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return new ReadOnlyCollection<Vehicle>(Parking.Vehicles);
        }

        public string ReadFromLog()
        {
            return LogService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = FindVehicleById(vehicleId);
            if(vehicle != null)
            {
                if(vehicle.Balance < 0)
                {
                    throw new InvalidOperationException("You have a negative Balance! Pay off the debt to take back the car!");
                }
                else
                {
                    Parking.Vehicles.Remove(vehicle);
                }
            }
            else
            {
                throw new ArgumentException("Such vehicle doesn't exist"!);
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            Vehicle.IdValidation(vehicleId);
            var vehicle = FindVehicleById(vehicleId);
            if(vehicle != null)
            {
                if(sum < 0)
                {
                    throw new ArgumentException("The sum for topping up is negative!");
                }
                else 
                {
                    FindVehicleById(vehicleId).Balance += sum;
                }
            }
            else
            {
                throw new NullReferenceException("The vehicle with such id doesn't exist!");
            }
        }

        public Vehicle FindVehicleById(string vehicleId)
        {
            Vehicle.IdValidation(vehicleId);
            var vehicle = Parking.Vehicles.Find(v => v.Id == vehicleId);
            if (vehicle == null)
            {
                throw new NullReferenceException("There is no vehicle with such Id!");
            }
            return Parking.Vehicles.Find(v => v.Id == vehicleId);
        }

        public decimal GetTariffById(VehicleType vehicleType)
        {
            return Parking.Tariffs[vehicleType];
        }

        public decimal GetFeeCoef()
        {
            return Parking.FeeCoeficient;
        }

        public void OnWithdraw(object sender, ElapsedEventArgs e)
        {
            foreach (var vehicle in Parking.Vehicles)
            {
                var transaction = new TransactionInfo(vehicle.Id, this);
                vehicle.Balance -= transaction.Sum;
                Parking.Balance += transaction.Sum;
                Parking.Transactions.Add(transaction);
            }
        }

        private void OnLog(object sender, ElapsedEventArgs e)
        {
            var logInfo = new StringBuilder();
            foreach (var transaction in GetLastParkingTransactions())
            {
                logInfo.Append(transaction.ToString() + "\n");
            }
            Parking.Transactions.Clear();
            LogService.Write(logInfo.ToString());
        }

    }
}

